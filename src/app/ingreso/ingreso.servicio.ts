import { Ingreso } from "./ingreso.model";

export class IngresoServicio{
    ingresos: Ingreso[]=[
        new Ingreso("salario", 4000),
        new Ingreso("venta de carro", 600)
    ];

    eliminar(ingreso:Ingreso){
        const indice: number = this.ingresos.indexOf(ingreso);
        this.ingresos.splice(indice,1);
    }
}

// aqui se cria a informação que são os valores das variaveis
// aqui se criou manualmente, mas ela pode ser dinamica